﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSprite
{
    public bool isWild;
    public int CardSuit;
    public int CardVal;
    public string CardString;
    public Sprite CardSpriteImage;
}
