﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;
using System.Threading;


public class CreditsText : MonoBehaviour
{
    int curCredits;
    int newCredits;
    int LastWin;
    public float CreditsTextUpdateSpeed = 0.05f;
    public static CreditsText creditsText;

    public Text Credits;
    public Text LastWinText;

    // Start is called before the first frame update
    void Start()
    {
        creditsText = this;
    }

    // Update is called once per frame
    void Update()
    {
        if(curCredits!=newCredits)
            StartCoroutine(UpdateCreditsText( () =>
            {
            }));

    }

    public void ModifyCredits(int newAmount)
    {
        newCredits = newAmount;
    }


    public void SetLastWin(int Amount)
    {
        LastWin = Amount;
        LastWinText.text = "LAST WIN " + LastWin.ToString();
    }


    public IEnumerator UpdateCreditsText( Action done)
    {
        WaitForSeconds waitTime = new WaitForSeconds(CreditsTextUpdateSpeed);
        while (newCredits != curCredits)
        {
            if (curCredits > newCredits)
                curCredits--;
            else
                curCredits++;
            Credits.text = "CREDITS " + curCredits.ToString();
            yield return waitTime;
        }
        done();
    }


}
