﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class DisplayCards
{
    public Text HoldText;
    public Image CardImage;
    public Button CardButton;
}



public class GameCard : MonoBehaviour
{
    public static GameCard gameCardDisplay;

    public DisplayCards[] CardsOnDisplay = new DisplayCards[5];


    // Start is called before the first frame update
    void Start()
    {
        gameCardDisplay = this;
    }


    //show hold text for a the card passed.
    public void EnableCardHoldText(int card, bool OnOff)
    {
        CardsOnDisplay[card].HoldText.enabled = OnOff;  
    }


    // will copy sprite to the cards on the display for all 5 cards
    public void UpdateCards( List<CardsInPlay> GameCards)
    {
        for (int i = 0; i < GameCards.Count; i++)
        {
            CardsOnDisplay[i].CardImage.sprite = GameCards[i].DisplayCard.CardSpriteImage;
        }
    }


    // enable or disable all cards buttons..
    public void EnableCardButtons(bool enable)
    {
        for (int i = 0; i < CardsOnDisplay.Length; i++)
        {
            CardsOnDisplay[i].CardButton.interactable = enable;
        }
    }

}
