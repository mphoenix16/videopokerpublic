﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public  class Cards : MonoBehaviour
{
    public static int MaxDecks = 1;  // video poker uses one deck....
    public static int CardsPerDeck = 52;
    public static int MaxCards = MaxDecks * CardsPerDeck;

    public static Cards cards;
    private  List<CardSprite> CardSpriteList = new List<CardSprite>();
    private  List<int> RandomNumbers = new List<int>();
    public  int cardsOutCount = 0; // used for debugging 

     string[] cardLookup = {
        "A",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "T",
        "J",
        "Q",
        "K",
    };
     string[] SuitLookup = {
        "S",
        "D",
        "H",
        "C",
    };


    // Start is called before the first frame update
    void Start()
    {
        cards = this;
    }



    // make a list of random numbers for each deck to use as a index into cardsprites;
    public  void SetupAndShuffleRandomNumbers()
    {
        RandomNumbers.Clear();
        for (int d = 0; d < MaxDecks; d++)
            for (int c = 0; c < MaxCards; c++)
                RandomNumbers.Add(c);
        
        Shuffle(RandomNumbers);
    }

    // shuffle the random number list
    // this list will be the order the cards are pulled from the 
    // deck and is used to index into CardSpriteList list
    public  void Shuffle(List<int> randNumbersList)
    {
        Random rng = new Random();
        int nRandom = 0;
        int saveValue;
        for (int n = 0; n < randNumbersList.Count; n++)
        {
            nRandom = Random.Range(0, randNumbersList.Count);
            saveValue = randNumbersList[n];
            randNumbersList[n] = randNumbersList[nRandom];
            randNumbersList[nRandom] = saveValue;
        }
    }


    // Get all card sprites from resources and set up CardSprite values
    public  void LoadCards()
    {
        int index = 0;
        CardSprite c;

        Sprite[] CardList = Resources.LoadAll<Sprite>("Cards");

        for (int s = 0; s < SuitLookup.Length; s++)
            for (int i = 0; i < cardLookup.Length; i++)
            {
                c = new CardSprite();
                c.isWild = false;
                c.CardSuit = s;
                c.CardVal = i;
                c.CardString = cardLookup[i] + SuitLookup[s];  // for easy debuging:)
                c.CardSpriteImage = CardList[index++];
                CardSpriteList.Add(c);
            }

        // this should be card sprite 52 
        // add one card back Red
        c = new CardSprite();
        c.CardSuit = 0;
        c.CardVal = 0;
        c.CardString = "back";
        c.CardSpriteImage = CardList[54];
        CardSpriteList.Add(c);


    }


    // pull a random number off the top of the list 
    // since we will only use one deck and suffle after every game we will never run out of cards.
    public  int GetNextRandom()
    {
        int c = RandomNumbers[0];
        RandomNumbers.RemoveAt(0);
        cardsOutCount++;
        return c;
    }

    // return the CardSprite for any card c
    public  CardSprite getCardSprite(int c)
    {
        return CardSpriteList[c];
    }
}

