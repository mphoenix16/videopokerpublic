﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;


public class OddsTable : MonoBehaviour
{

    public static OddsTable oddsTable;
    public Image[] Odds = new Image[5]; 


    public int[,] PayTable = {

        {0250,050,025,09,06,04,03,02,01},
        {0500,100,050,18,12,08,06,04,02},
        {0750,150,075,27,18,12,09,06,03},
        {1000,200,100,36,24,16,12,08,04},
        {4000,250,125,45,30,20,15,10,05},

    };

    // Start is called before the first frame update
    void Start()
    {
        oddsTable = this;
        SetOddsTableTextValues();
    }


    public int getWinPayOut(int Hand,int Bet)
    {
        return  PayTable[Bet - 1, Hand];
    }

    // set the odds table highlight based on the bet
    public void SetOddsHighlight(int Bet)
    {
        for (int i = 0; i < Odds.Length; i++)
        {
            if (i == (Bet - 1))
                Odds[i].color = Color.red;
            else
                Odds[i].color = Color.black;
        }
    }


    // this is slow so only do at start of game;
    public void SetOddsTableTextValues()
    {
        for (int o = 0; o < Odds.Length; o++)
        {
            OddsText oddsT = Odds[o].gameObject.GetComponent<OddsText>();
            for (int i = 0; i < oddsT.OddsTableText.Length; i++)
            {
                oddsT.OddsTableText[i].text = PayTable[o, i].ToString();
            }
        }
    }

}
