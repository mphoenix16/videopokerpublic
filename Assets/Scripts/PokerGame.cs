﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class PokerGame : MonoBehaviour
{
    public static int MaxBet = 5;
    public bool ShuffleEachHand = true;
    private int CurBet = 1;
    private int Credits = 500;
   
    enum GameState  {
        GameWait,
        GameInit,
        GameDeal,
        GameDrawNewCards,
        GameOver,
        GameWin,
        GameLoose,
        GameWaitForPlayerHold,
        GameStartNewHand,
        GameEvaluate,
    }

    GameState CurGameState;


    public Text BetText;
    public Button MaxBetButton;
    public Button BetOneButton;
    public Button HoldButton;
    public Text MessageText;
      

    private string[] TextHand =
    {

        "ROYAL FLUSH",
        "STRAIGHT FLUSH",
        "FOUR OF A KIND",
        "FULL HOUSE",
        "FLUSH",
        "STRAIGHT",
        "THREE OF A KIND",
        "TWO PAIR",
        "JACKS OR BETTER",
        "FIVE OF A KIND",
        "GAME OVER",
    };




    // Start is called before the first frame update
    void Start()
    {        
        CurGameState = GameState.GameInit;
    }



    // simple state machine
    // Update is called once per frame
    void Update()
    {
        switch(CurGameState)
        {
            case GameState.GameDeal:
                break;

            case GameState.GameInit:
                
                // set up the display for the first time.
                CurGameState = GameState.GameWait;
                Cards.cards.LoadCards();
                Cards.cards.SetupAndShuffleRandomNumbers();

                Poker.poker.InitPoker();
                BetText.text = CurBet.ToString();
                DisplayMessage("", false);
                OddsTable.oddsTable.SetOddsHighlight(CurBet);
                CreditsText.creditsText.SetLastWin(0);
                CreditsText.creditsText.ModifyCredits(Credits);

                StartCoroutine(Poker.poker.ShowCardBacksFirst(true,() =>
                {
                    CurGameState = GameState.GameOver;
                    GameCard.gameCardDisplay.EnableCardButtons(false);
                    EnableBetButtons(true);
                    DisplayMessage("GAME OVER", true);
                    Poker.poker.ClearHoldText();
                }));
                break;

            case GameState.GameWait:
                break;

            case GameState.GameLoose:
                break;

            case GameState.GameOver:
                break;

            case GameState.GameEvaluate:
                Poker.handType hand = Poker.poker.checkCardList();
                GameCard.gameCardDisplay.EnableCardButtons(false);
                DisplayMessage(TextHand[(int)hand], true);
                if (hand == Poker.handType.NONE)
                {
                    
                    CurGameState = GameState.GameLoose;
                }
                else
                {
                    CurGameState = GameState.GameWin;
                    GiveUserWinnings((int)hand, CurBet);

                   
                }
                break;

            case GameState.GameWaitForPlayerHold:
                break;

            case GameState.GameWin:
                break;

            case GameState.GameStartNewHand:


                CurGameState = GameState.GameWait;
                if(ShuffleEachHand)
                    Cards.cards.SetupAndShuffleRandomNumbers();
                Poker.poker.ClearHoldText();
                DisplayMessage("", false);
                EnableBetButtons(false);
                GameCard.gameCardDisplay.EnableCardButtons(true);
                StartCoroutine(Poker.poker.DealPlayerCards(true, () =>
                {
                    CurGameState = GameState.GameWaitForPlayerHold;
                                      
                }));

                break;

            case GameState.GameDrawNewCards:
                CurGameState = GameState.GameWait;

                StartCoroutine(Poker.poker.DealPlayerCards(false, () =>
                {
                    EnableBetButtons(true);
                    CurGameState = GameState.GameEvaluate;
                    Poker.poker.ClearHoldText();
 

                }));

                break;

        }
    }


    // take the bet amount from the users credits and update credits text
    public bool TakeBetFromCreditsAndUpdate(int curBet)
    {
        bool result = false;
        Credits -= curBet;
        if (Credits > 0)
            result =  true;
        else
            Credits += curBet;
        CreditsText.creditsText.ModifyCredits(Credits);
        return result;
    }


    //giv the user the winnings for the lats hand and update credits tect
    public void GiveUserWinnings(int Hand,int curBet)
    {
        int winAmount = OddsTable.oddsTable.getWinPayOut(Hand, CurBet);
        CreditsText.creditsText.SetLastWin(winAmount);
        Credits += winAmount;
        CreditsText.creditsText.ModifyCredits(Credits);
    }


    // message is used for displaying hand text when you win
    public  void DisplayMessage(string message, bool OnOff)
    {
        MessageText.enabled = OnOff;
        MessageText.text = message;
    }


    // when the user presses the Draw Button set the game state
    public void ButtonDraw()
    {
        if (CurGameState == GameState.GameWaitForPlayerHold)
            CurGameState = GameState.GameDrawNewCards;
        else
        {
            if(TakeBetFromCreditsAndUpdate(CurBet))
                CurGameState = GameState.GameStartNewHand;
        }
    }

    //Toggle bet buttons on/off
    public void EnableBetButtons(bool enable)
    {
        MaxBetButton.interactable = enable;
        BetOneButton.interactable = enable;
    }

    // set the bet to max bet 
    //update onscreen bet text
    //update odds highlight
    public void ButtonBetMax()
    {
        CurBet = MaxBet;
        BetText.text = CurBet.ToString();
        OddsTable.oddsTable.SetOddsHighlight(CurBet);
    }

    //inc bet by 1 until max bet then set back to 1
    //update onscreen bet text
    //update odds highlight
    public void ButtonBetOne()
    {
        CurBet++;
        if (CurBet > MaxBet)
            CurBet = 1;
        BetText.text = CurBet.ToString();
        OddsTable.oddsTable.SetOddsHighlight(CurBet);
    }


    // toggle the Hold text on the card selected
    public void ButtonCard(int card)
    {
        Poker.poker.ChangeCardHoldText(card);
    }


}
