﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;
using System.Threading;

// used to hold the card that is in play.
public class CardsInPlay{
    public CardSprite DisplayCard;
    public bool Held;
    public bool AlreadyCounted;
}


public class Poker : MonoBehaviour
{
    private  handType CurHand = handType.NONE;
    public static Poker poker;

    public static int CardBackSpriteIndex = 52;
    public float DealSpeed = 0.2f;
    public  int MaxCardsPerHand = 5;
    public  List<CardsInPlay> cardsInPlay = new List<CardsInPlay>();


    public static int[] debug = {
       8,9,10,11,12
    };

    public  enum handType
    {
        ROYALFLUSH,
        STRAIGHTFLUSH,
        FOUROFAKIND,
        FULLHOUSE,
        FLUSH,
        STRAIGHT,
        THREEOFAKIND,
        TWOPAIR,
        ONEPAIR,
        FIVEOFAKIND,
        NONE,
    };


    // Start is called before the first frame update
    void Start()
    {
        poker = this;
        // set up cards in play list
    }


    //set up cards in play list
    public void InitPoker()
    {
        for (int i = 0; i < MaxCardsPerHand; i++)
        {
            CardsInPlay nc = new CardsInPlay();
            nc.DisplayCard = Cards.cards.getCardSprite(CardBackSpriteIndex);  
            cardsInPlay.Add(nc);
            GameCard.gameCardDisplay.UpdateCards(Poker.poker.cardsInPlay);
        }
    }



    // deal 5 cards to player if it is a new game clear the card list
    public  IEnumerator  DealPlayerCards(bool NewDeal,Action done)
    {
        WaitForSeconds waitTime = new WaitForSeconds(DealSpeed);

        yield return StartCoroutine(ShowCardBacksFirst(NewDeal, () => { }));
 
        for (int i = 0; i < MaxCardsPerHand; i++)
        {
            if (!cardsInPlay[i].Held)
            {
                cardsInPlay[i].DisplayCard = Cards.cards.getCardSprite(Cards.cards.GetNextRandom());
                GameCard.gameCardDisplay.UpdateCards(Poker.poker.cardsInPlay);
                yield return waitTime;

            }
        }
 
        done();
    }


    // this will set up the cards in play list and show card backs
    public IEnumerator ShowCardBacksFirst(bool NewDeal , Action done)
    {
        WaitForSeconds waitTime = new WaitForSeconds(0.01f);
        for (int i = 0; i < MaxCardsPerHand; i++)
        {
            if (!cardsInPlay[i].Held)
            {
                cardsInPlay[i].DisplayCard = Cards.cards.getCardSprite(CardBackSpriteIndex);
                GameCard.gameCardDisplay.UpdateCards(Poker.poker.cardsInPlay);
                yield return waitTime;

            }
        }
        done();
    }



    // clear card hold flag and reset the text on the card
    public  void ClearHoldText()
    {
        for (int i = 0; i < cardsInPlay.Count; i++)
        {
            cardsInPlay[i].Held = true;
            ChangeCardHoldText(i);
        }
    }


    public  void ChangeCardHoldText(int card)
    {
        cardsInPlay[card].Held = !cardsInPlay[card].Held;
        GameCard.gameCardDisplay.EnableCardHoldText(card, cardsInPlay[card].Held);
    }




    // make a new list with sorted cards to evaluate so we don't mess up the user cards order.
    // This is just a brute force evaluater so it easy to debug as i want to get this done fast.
    // 0 = ace
    // 1 = 2
    // 2 = 3
    // 3 = 4
    // 4 = 5
    // 5 = 6
    // 6 = 7
    // 7 = 8
    // 8 = 9
    // 9 = 10
    // 10 = j
    // 11 = q
    // 12 = k

    public List<CardsInPlay> SortedCards;
    public handType checkCardList()
    {

        SortedCards = new List<CardsInPlay>(cardsInPlay);
        SortedCards = SortedCards.OrderBy(o => o.DisplayCard.CardVal).ToList();
        foreach (CardsInPlay cip in SortedCards)
            cip.AlreadyCounted = false;


        CurHand = handType.NONE;

        if (checkFlush2() )
        {
            CurHand = handType.FLUSH;
            if ( checkStraight2())
            {
                CurHand = handType.STRAIGHTFLUSH;
                if (getCardVal(0) == 0)
                    CurHand = handType.ROYALFLUSH;

            }
            return CurHand;
        }
        else if(checkStraight2())
        {
            CurHand = handType.STRAIGHT;
            return CurHand;
        }
        else if (checkPairs())
        {
            return CurHand;
        }

        return handType.NONE;
    }


    // helper to get suit
    public  int getSuit(int card)
    {
        return SortedCards[card].DisplayCard.CardSuit;
    }

    //Helper to get card value
    public  int getCardVal(int card)
    {
        return SortedCards[card].DisplayCard.CardVal;
    }

    //Helper return wild
    public bool isCardWild(int card)
    {
        return SortedCards[card].DisplayCard.isWild;
    }



    public int GetWildCount()
    {
        int wilds = 0;
        for (int i = 0; i < cardsInPlay.Count; i++)
        {
            if (SortedCards[i].DisplayCard.isWild)
                wilds++;
        }
        return wilds;
  }

    // check for a flush get the suit of each card;
    public bool checkFlush2()
    {
        int[] suitList = new int[4];
        int wilds = GetWildCount();
        for (int i = 0; i < cardsInPlay.Count; i++)
        {
            if (!isCardWild(i))
                suitList[getSuit(i)]++;
        }

        for (int i = 0; i < suitList.Length; i++)
        {
            if ((suitList[i] + wilds) > 4)
                return true;
        }
        return false;
    }




    // look for straight including ace high
    public bool checkStraight2()
    {
        int wilds = GetWildCount();
        int missingCards = 0;
        int prevCard;
        if (getCardVal(0) != 0)  // ace?
        {
            prevCard = getCardVal(0);
            for (int i = 1; i < cardsInPlay.Count; i++)
            {
                if ((getCardVal(i) != prevCard +1) ) 
                {
                    missingCards++;
                }
                else if(isCardWild(i))
                {
                    wilds--;
                }
                prevCard = getCardVal(i);
            }

        }
        else
        {
            //skip the first card since we know it is a ACE
            for (int i = 1; i < cardsInPlay.Count; i++)
            {
                if (getCardVal(i) != 8 + i) // 9= card 10 --- card 10 - k we already know about the ACE.
                {
                    missingCards++;
                }
                else if (isCardWild(i))
                {
                    wilds--;
                }
           } 
        }
        if (missingCards - wilds == 0)
            return true;
        return false;

    }




    bool JackOrBetter = false;

    //take a card and count how many times it shows up in the players hand.
    public int CountHowMany(int StartCard)
    {
        int MatchCount = 0;
        int MatchVal = getCardVal(StartCard);
        for (int i = StartCard + 1; i < SortedCards.Count; i++)
        {
            if (MatchVal == getCardVal(i) && !SortedCards[i].AlreadyCounted )
            {
                // 10 is the value of a JACK and 0 is the value of a ACE
                if ( (MatchVal > 9 || MatchVal == 0))
                    JackOrBetter = true;
                SortedCards[i].AlreadyCounted = true;    // so we don't count this card again (reset each time we evaluate the hand)
                MatchCount++;
            }
        }
        return MatchCount;
    }


    // pairList[0] = count of no matches
    // pairList[1] = count of pairs
    // pairList[2] = count of three a kinds
    // pairList[3] = count of four of a kind
    // pairList[4] = count of five of a kind 

    public bool checkPairs()
    {
        JackOrBetter = false;
        int wilds = GetWildCount();
        int[] pairList = new int[5];

        for (int i = 0; i < SortedCards.Count; i++)
        {           
            pairList[CountHowMany(i)]++;
        }

        if (pairList[4] > 0)
        {
            CurHand = handType.FIVEOFAKIND;
            return true;
        }
        if (pairList[3] > 0)
        {
            CurHand = handType.FOUROFAKIND;
            return true;
        }
        else if (pairList[2] > 0 && pairList[1] > 0)
        {
            CurHand = handType.FULLHOUSE;
            return true;
        }
        else if (pairList[2] > 0)
        {
            CurHand = handType.THREEOFAKIND;
            return true;
        }
        else if (pairList[1] > 1)
        {
            CurHand = handType.TWOPAIR;
            return true;
        }
        else if (pairList[1] > 0)
        {
            if (JackOrBetter)
            {
                CurHand = handType.ONEPAIR;
                return true;
            }
        }
        return false;

    }
  
}
